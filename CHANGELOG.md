## [0.1.0] - 2022-06-29

- Initial release

## [0.1.1] - 2022-06-30

- Add error handling
- Add ability to add opts to Spn2.save

## [0.1.2] - 2022-07-02

- Add user_status
- Add status calls for multiple job_ids and outlinks

## [0.2.0] - 2022-07-03

- Breaking change: Single method 'status' with kwarg :job_ids now used for status of job(s)