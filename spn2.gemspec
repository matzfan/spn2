# frozen_string_literal: true

require_relative 'lib/spn2/version'

Gem::Specification.new do |spec|
  spec.name = 'spn2'
  spec.version = Spn2::VERSION
  spec.authors = ['MatzFan']

  spec.summary = 'Gem for the Save Page Now 2 API of the Wayback Machine'
  spec.description = 'Automate the process of saving web pages to archive.org'
  spec.homepage = 'https://gitlab.com/matzfan/spn2'
  spec.license = 'MIT'
  spec.required_ruby_version = '>= 3.1'

  spec.metadata['allowed_push_host'] = 'https://rubygems.org'
  spec.metadata['homepage_uri'] = 'https://gitlab.com/matzfan/spn2'
  spec.metadata['source_code_uri'] = 'https://gitlab.com/matzfan/spn2'
  spec.metadata['changelog_uri'] = 'https://gitlab.com/matzfan/spn2/CHANGELOG.md'

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files = Dir.chdir(__dir__) do
    `git ls-files -z`.split("\x0").reject do |f|
      (f == __FILE__) || f.match(%r{\A(?:(?:bin|test|spec|features)/|\.(?:git|travis|circleci)|appveyor)})
    end
  end
  spec.bindir = 'exe'
  spec.executables = spec.files.grep(%r{\Aexe/}) { |f| File.basename(f) }
  spec.require_paths = ['lib']

  spec.add_dependency 'curb', '~> 1.0'
  spec.add_dependency 'nokogiri', '~> 1.13'

  spec.add_development_dependency 'guard', '~> 2.18'
  spec.add_development_dependency 'guard-minitest', '~> 2.4'
  spec.add_development_dependency 'minitest', '~> 5.16'
  spec.add_development_dependency 'minitest-parallel_fork', '~> 1.2'
  spec.add_development_dependency 'rake', '~> 13.0'
  spec.add_development_dependency 'rubocop', '~> 1.30'
  spec.add_development_dependency 'rubocop-minitest', '~> 0.20'
  spec.add_development_dependency 'rubocop-rake', '~> 0.6'

  spec.metadata['rubygems_mfa_required'] = 'true'
end
