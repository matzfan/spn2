# frozen_string_literal: true

require 'minitest/autorun'
require 'minitest/parallel_fork'
require 'securerandom'

require 'spn2'
require 'spn2/version'

ALL_STATUS_KEYS = %w[counters
                     duration_sec
                     http_status
                     job_id
                     original_url
                     outlinks
                     resources
                     status
                     timestamp].freeze

BAD_AUTH_MSG_HASH_STRING = { 'message' => Spn2::BAD_AUTH_MSG }.to_s
ERROR_CLASSES = %w[Spn2::Spn2Error
                   Spn2::Spn2Error400
                   Spn2::Spn2Error502
                   Spn2::Spn2ErrorBadAuth
                   Spn2::Spn2ErrorBadParams
                   Spn2::Spn2ErrorFailedCapture
                   Spn2::Spn2ErrorInvalidOption
                   Spn2::Spn2ErrorMissingKeys
                   Spn2::Spn2ErrorNoOutlinks
                   Spn2::Spn2ErrorTooManyRequests
                   Spn2::Spn2ErrorUnknownResponse
                   Spn2::Spn2ErrorUnknownResponseCode].freeze

def expl
  'example.com'
end

def job_id
  'spn2-9c17e047f58f9220a7008d4f18152fee4d111d14' # http://example.com/
end

def bad_auth_header
  { Authorization: "LOW #{SecureRandom.urlsafe_base64(12)}:#{SecureRandom.urlsafe_base64(12)}" }
end

def string_to_hash(hash_as_string)
  JSON.parse(hash_as_string.gsub('=>', ':').gsub(':nil,', ':null,'))
rescue JSON::ParserError
  nil
end
