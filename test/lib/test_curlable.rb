# frozen_string_literal: true

require 'curlable'

require 'test_helper'

class Includer
  include Curlable
end

class TestCurlable < Minitest::Test
  def self.includer
    @includer ||= Includer.new
  end

  def includer
    self.class.includer
  end

  def url_get
    'httpbin.org/get'
  end

  def url_post
    'httpbin.org/post'
  end

  def test_get
    assert_equal "http://#{url_get}", JSON.parse(includer.get(url: url_get))['url']
  end

  def test_get__with_header
    resp = includer.get(url: url_get, headers: { 'Authorization' => 'auth' })
    assert_equal 'auth', JSON.parse(resp)['headers']['Authorization']
  end

  def test_post
    hash = JSON.parse(includer.post(url: url_post, params: { k1: 'v1', k2: 'v2' }))
    assert_equal 'http://httpbin.org/post?k1=v1&k2=v2', hash['url']
    assert_equal Hash[*%w[k1 v1 k2 v2]], hash['args']
  end

  def test_post__with_header
    resp = includer.post(url: url_post, headers: { 'Authorization' => 'auth' })
    assert_equal 'auth', JSON.parse(resp)['headers']['Authorization']
  end
end
