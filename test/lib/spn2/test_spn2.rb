# frozen_string_literal: true

require 'securerandom'

require 'test_helper'

class TestSpn2 < Minitest::Test
  def too_many_requests_html
    @too_many_requests_html ||= File.read('test/fixtures/too_many_requests.html')
  end

  def bad_request_html
    @bad_request_html ||= File.read('test/fixtures/400_bad_request.html')
  end

  def test__public_interface
    assert_equal %i[access_key capture error_classes save secret_key status system_status user_status],
                 Spn2.methods(false).sort
  end

  def test_error_classes
    assert_equal ERROR_CLASSES, Spn2.error_classes.map(&:to_s).sort
  end

  def test_version
    refute_nil Spn2::VERSION
  end

  def test_auth_keys
    refute_nil ENV.fetch('SPN2_ACCESS_KEY'), Spn2.access_key
    refute_nil ENV.fetch('SPN2_SECRET_KEY'), Spn2.secret_key
  end

  def test_system_status
    refute_empty Spn2.system_status['status']
  end

  def test_user_status
    assert_equal %w[available daily_captures daily_captures_limit processing], Spn2.user_status.keys.sort
  end

  # Private methods

  def test_status_params
    assert_equal({ job_id: job_id }, Spn2.send(:status_params, job_ids: job_id, outlinks: false))
    assert_equal({ job_id_outlinks: job_id }, Spn2.send(:status_params, job_ids: job_id, outlinks: true))
    assert_equal({ job_ids: "#{job_id},#{job_id}" },
                 Spn2.send(:status_params, job_ids: [job_id, job_id], outlinks: false))
  end

  def test_json__with_valid_json
    assert_equal({ 'k' => 'v' }, Spn2.send(:json, '{ "k":"v" }'))
  end

  def test_json__with_html_with_title_calls_parse_error_code_from_page_title
    html_str = '<html><title>502 Bad Gateway</title></html>'
    mock = MiniTest::Mock.new # https://stackoverflow.com/questions/39889537/stub-and-mock-minitest
    mock.expect(:parse_error_code_from_page_title, nil, ['502 Bad Gateway']) # response nil, args []
    Spn2.stub :parse_error_from_page_body, nil do # must stub this otherwise will be last call!
      Spn2.stub(:parse_error_code_from_page_title, ->(a) { mock.parse_error_code_from_page_title(a) }) do
        Spn2.send(:json, html_str)
      end
    end
    mock.verify
  end

  def test_json__with_html_with_no_title_calls_parse_error_from_page_body
    html_str = '<html><h1></h1></html>'
    mock = MiniTest::Mock.new
    mock.expect(:parse_error_from_page_body, nil, ['<html><h1></h1></html>'])
    Spn2.stub(:parse_error_from_page_body, ->(a) { mock.parse_error_from_page_body(a) }) do
      Spn2.send(:json, html_str)
    end
    mock.verify
  end

  def test_parse_error_code_from_page_title
    assert_raises(Spn2::Spn2Error502) { Spn2.send(:parse_error_code_from_page_title, '502 Bad Gateway') }
    assert_raises(Spn2::Spn2ErrorUnknownResponseCode) { Spn2.send(:parse_error_code_from_page_title, 'Foo') }
  end

  def test_parse_error_from_page_body
    assert_raises(Spn2::Spn2ErrorTooManyRequests) { Spn2.send(:parse_error_from_page_body, too_many_requests_html) }
    assert_raises(Spn2::Spn2Error400) { Spn2.send(:parse_error_from_page_body, bad_request_html) }
    assert_raises(Spn2::Spn2ErrorUnknownResponse) do
      Spn2.send(:parse_error_from_page_body, '<html><h2>Some html</h2></html>')
    end
  end
end
