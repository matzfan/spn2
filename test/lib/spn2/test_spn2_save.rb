# frozen_string_literal: true

require 'test_helper'

class TestSpn2 < Minitest::Test
  def test_save__with_bad_auth
    Spn2.stub(:auth_header, bad_auth_header) do
      exception = assert_raises Spn2::Spn2ErrorBadAuth do
        Spn2.save(url: expl)
      end
      assert_equal(BAD_AUTH_MSG_HASH_STRING, exception.message)
    end
  end

  def test_save__returns_no_job_id
    Spn2.stub(:auth_post, '{"message":"no job_id key!"}') do
      e = assert_raises Spn2::Spn2ErrorFailedCapture do
        Spn2.save(url: expl)
      end
      assert_equal('{"message"=>"no job_id key!"}', e.message)
    end
  end

  def test_save
    assert_match Spn2::JOB_ID_REGEXP, Spn2.save(url: expl)['job_id']
  end

  def test_save__with_options
    assert_raises(Spn2::Spn2ErrorInvalidOption) { Spn2.save(url: expl, opts: { notanoption: 1 }) }
    opts = { capture_outlinks: 1, js_behavior_timeout: 3 }
    assert_match Spn2::JOB_ID_REGEXP, Spn2.save(url: expl, opts: opts)['job_id']
  end
end
