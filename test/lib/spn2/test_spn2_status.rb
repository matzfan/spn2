# frozen_string_literal: true

require 'securerandom'

require 'test_helper'

class TestSpn2 < Minitest::Test
  def another_job_id
    'spn2-0a70c3aecd3383184da1e193c82e83f2412335c5' # ipecho.net/plain has no outlinks
  end

  def test_status__with_bad_auth
    Spn2.stub(:auth_header, bad_auth_header) do
      exception = assert_raises Spn2::Spn2ErrorBadAuth do
        Spn2.status(job_ids: job_id)
      end
      assert_equal(BAD_AUTH_MSG_HASH_STRING, exception.message)
    end
  end

  def test_status
    keys = Spn2.status(job_ids: job_id).keys
    assert_empty Spn2::ESSENTIAL_STATUS_KEYS - keys # at least these keys
    assert_empty keys - ALL_STATUS_KEYS # no more than these keys
  end

  def test_status__response_missing_essential_keys
    Spn2.stub(:auth_post, '{"job_id":"bad-job-id","resources":"[]"}') do
      e = assert_raises Spn2::Spn2ErrorMissingKeys do
        Spn2.status(job_ids: job_id)
      end
      assert_equal('{"job_id"=>"bad-job-id", "resources"=>"[]"}', e.message)
    end
  end

  def test_status__with_array_of_job_ids
    response = Spn2.status(job_ids: [another_job_id, job_id])
    response.each do |e|
      assert_empty Spn2::ESSENTIAL_STATUS_KEYS - e.keys # at least these keys
      assert_empty e.keys - ALL_STATUS_KEYS # no more than these keys
    end
  end

  def test_status__with_array_of_job_ids_non_array_reponse
    Spn2.stub(:auth_post, '{"message":"Some fail message"}') do
      e = assert_raises Spn2::Spn2Error do
        Spn2.status(job_ids: [job_id, 'another_job_id']) # remove stub once a real failure can be tested
      end
      assert_equal('{"message"=>"Some fail message"}', e.message)
    end
  end

  def test_status__with_outlinks_set_to_true
    wiki_job_id = Spn2.save(url: 'en.wikipedia.org/wiki/Special:Random', opts: { capture_outlinks: 1 })['job_id']
    sleep 2 # wait for Wayback Machine
    resp = Spn2.status(job_ids: wiki_job_id, outlinks: true)
    assert_equal Array, resp.class
  end

  def test_status__with_outlinks_set_to_true_no_outlinks
    e = assert_raises(Spn2::Spn2ErrorNoOutlinks) { Spn2.status(job_ids: another_job_id, outlinks: true) }
    assert_equal({ 'message' => 'No job_ids found.', 'status' => 'error' }, string_to_hash(e.message))
  end
end
