# frozen_string_literal: true

# namespace
module Spn2
  BAD_AUTH_MSG = 'You need to be logged in to use Save Page Now.'
  ERROR_CODES = [400, 502].freeze
  TOO_MANY_REQUESTS = 'Too Many Requests'

  class Spn2Error < StandardError; end
  class Spn2ErrorBadAuth < Spn2Error; end
  class Spn2ErrorBadParams < Spn2Error; end
  class Spn2ErrorFailedCapture < Spn2Error; end
  class Spn2ErrorInvalidOption < Spn2Error; end
  class Spn2ErrorMissingKeys < Spn2Error; end
  class Spn2ErrorNoOutlinks < Spn2Error; end
  class Spn2ErrorTooManyRequests < Spn2Error; end
  class Spn2ErrorUnknownResponse < Spn2Error; end
  class Spn2ErrorUnknownResponseCode < Spn2Error; end
  ERROR_CODES.each { |i| const_set("Spn2Error#{i}", Class.new(Spn2Error)) }
end
