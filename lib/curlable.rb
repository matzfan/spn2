# frozen_string_literal: true

require 'curb'

# namespace
module Curlable
  def get(url:, headers: {})
    Curl::Easy.http_get(url) do |http|
      http.follow_location = true
      headers.each { |k, v| http.headers[k] = v }
    end.body_str
  end

  def post(url:, headers: {}, params: {})
    Curl::Easy.http_post("#{url}?#{Curl.postalize(params)}", Curl.postalize(params)) do |http|
      http.follow_location = true
      headers.each { |k, v| http.headers[k] = v }
    end.body_str
  end
end
