# frozen_string_literal: true

require 'date'
require 'json'
require 'nokogiri'

require_relative 'curlable'
require_relative 'spn2_errors'

# Design decison to not use a class as only 'state' is in 2 env vars
module Spn2
  extend Curlable

  ESSENTIAL_STATUS_KEYS = %w[job_id resources status].freeze
  JOB_ID_REGEXP = /^(spn2-([a-f]|\d){40})$/
  WEB_ARCHIVE = 'https://web.archive.org'

  BINARY_OPTS = %w[capture_all capture_outlinks capture_screenshot delay_wb_availabilty force_get skip_first_archive
                   outlinks_availability email_result].freeze
  OTHER_OPTS = %w[if_not_archived_within js_behavior_timeout capture_cookie target_username target_password].freeze

  class << self
    def error_classes
      Spn2.constants.map { |e| Spn2.const_get(e) }.select { |e| e.is_a?(Class) && e < Exception }
    end

    def access_key
      ENV.fetch('SPN2_ACCESS_KEY')
    end

    def secret_key
      ENV.fetch('SPN2_SECRET_KEY')
    end

    def system_status
      json get(url: "#{WEB_ARCHIVE}/save/status/system") # no auth
    end

    def user_status
      json auth_get(url: "#{WEB_ARCHIVE}/save/status/user?t=#{DateTime.now.strftime('%Q').to_i}")
    end

    def save(url:, opts: {})
      raise Spn2ErrorInvalidOption, "One or more invalid options: #{opts}" unless options_valid?(opts)

      json = json(auth_post(url: "#{WEB_ARCHIVE}/save/#{url}", params: { url: url }.merge(opts)))
      raise Spn2ErrorBadAuth, json.inspect if json['message']&.== BAD_AUTH_MSG

      raise Spn2ErrorFailedCapture, json.inspect unless json['job_id']

      json
    end
    alias capture save

    def status(job_ids:, outlinks: false)
      params = status_params(job_ids: job_ids, outlinks: outlinks)
      json = json(auth_post(url: "#{WEB_ARCHIVE}/save/status", params: params))
      return json if json.is_a? Array # must be valid response

      handle_status_errors(job_ids: job_ids, json: json, outlinks: outlinks)
      json
    end

    private

    def status_params(job_ids:, outlinks:)
      return { job_ids: job_ids.join(',') } if job_ids.is_a?(Array)
      return { job_id_outlinks: job_ids } if outlinks

      { job_id: job_ids } # single job_id
    end

    def handle_status_errors(job_ids:, json:, outlinks:)
      raise Spn2ErrorBadAuth, json.inspect if json['message']&.== BAD_AUTH_MSG
      raise Spn2ErrorNoOutlinks, json.inspect if outlinks
      raise Spn2ErrorMissingKeys, json.inspect unless (ESSENTIAL_STATUS_KEYS - json.keys).empty?
      raise Spn2Error, json.inspect if job_ids.is_a?(Array)
    end

    def auth_get(url:)
      get(url: url, headers: accept_header.merge(auth_header))
    end

    def auth_post(url:, params: {})
      post(url: url, headers: accept_header.merge(auth_header), params: params)
    end

    def accept_header
      { Accept: 'application/json' }
    end

    def auth_header
      { Authorization: "LOW #{Spn2.access_key}:#{Spn2.secret_key}" }
    end

    def doc(html_string)
      Nokogiri::HTML html_string
    end

    def json(html_string)
      JSON.parse(doc = doc(html_string))
    rescue JSON::ParserError # an html response
      parse_error_code_from_page_title(doc.title) if doc.title
      parse_error_from_page_body(html_string)
    end

    def parse_error_code_from_page_title(title_string)
      raise_code_response_error_if_code_in_string(title_string)
      raise Spn2ErrorUnknownResponseCode, title_string # code found but doesn't match any known error classes
    end

    def parse_error_from_page_body(html_string)
      h1_tag_text = h1_tag_text(html_string)
      raise_code_response_error_if_code_in_string h1_tag_text
      raise Spn2ErrorTooManyRequests if h1_tag_text == TOO_MANY_REQUESTS

      raise Spn2ErrorUnknownResponse, html_string # fall through
    end

    def h1_tag_text(html_string)
      doc(html_string).xpath('//h1')&.text || ''
    end

    def raise_code_response_error_if_code_in_string(string)
      return unless ERROR_CODES.include? code = string.to_i

      raise Spn2.const_get("Spn2Error#{code}")
    end

    def options_valid?(opts)
      opts.keys.all? { |k| (BINARY_OPTS + OTHER_OPTS).include? k.to_s }
    end
  end
end
